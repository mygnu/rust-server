/// EmailData holds a String property email
/// ## Example
///
/// ```rust
/// #[derive(Debug, Serialize, Deserialize)]
///  pub struct EmailData {
///    pub email: String
///    pub text: String
///  }
/// ```
#[derive(Debug, Serialize, Deserialize)]
pub struct EmailData {
    pub to: Option<String>,
    pub from: Option<String>,
    pub email: String,
    pub text: String,
}
