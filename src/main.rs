#[macro_use]
extern crate lazy_static;

#[macro_use]
extern crate serde_derive;


use actix_web::{http, middleware::Logger, server, App, HttpRequest, HttpResponse, Json};
use sparkpost::transmission::{Message, Transmission, TransmissionResponse};
use crate::email_data::EmailData;
use crate::settings::Settings;

mod email_data;
mod settings;

lazy_static! {
static ref sett: Settings =  Settings::new()
    .expect("Make sure you have config file and all its variable set.");
}

fn check(req: HttpRequest) -> HttpResponse {
    let email: Result<String, _> = req.match_info().query("email");
    match email {
        Ok(email) => {
            if mailchecker::is_valid(&email) {
                HttpResponse::Ok().json("OK")
            } else {
                HttpResponse::BadRequest().json("Invalid Email")
            }
        }
        _ => HttpResponse::BadRequest().json("Invalid Email"),
    }
}

fn send_email(data: Json<EmailData>) -> HttpResponse {
    let error_message = HttpResponse::BadRequest().json("Please provide a valid email");
    if mailchecker::is_valid(&data.email) {
        let tm = Transmission::new_eu(sett.sparkpost_api_key.as_str());
        //        let subject = format!("{} {}", &data.email, "submission from letsorganise.net");
        let mut message = Message::new(sett.sender_email.as_ref());
        message
            .subject("Email submission from letsorganise.net")
            .add_recipient(sett.receive_email.as_str())
            .text(data.text.as_str());
        let result = tm.send(&message);
        match result {
            Ok(ts_response) => match ts_response {
                TransmissionResponse::ApiResponse(api_res) => {
                    if api_res.total_rejected_recipients == 0 {
                        HttpResponse::Ok().json("Email successfully submitted")
                    } else {
                        error_message
                    }
                }
                TransmissionResponse::ApiError(_) => error_message,
            },
            Err(_) => error_message,
        }
    } else {
        error_message
    }
}

fn main() {
    std::env::set_var("RUST_LOG", "actix_web=info");
    env_logger::init();
    let sys = actix::System::new("send_email");
    let server_url = String::from("127.0.0.1:") + sett.server_port.as_str();

    server::new(move || {
        App::new()
            // enable logger
            .middleware(Logger::default())
            .resource("/submit", |r| r.method(http::Method::POST).with(send_email))
            .resource("/validate/{email}", |r| {
                r.method(http::Method::GET).with(check)
            })
            .finish()
    })
        .bind(server_url.as_str())
        .expect(&format!("Can not bind to {}", server_url.as_str()))
        .keep_alive(120)
        .shutdown_timeout(5)
        .start();
    println!("email-server running at {}", server_url.as_str());
    let _ = sys.run();
}
